package com.dangfei.daoimp;

import com.dangfei.dao.UserDao;
import com.dangfei.model.User;
import com.dangfei.dbconnect.Dbconnect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoimp implements UserDao {
    @Override
    public User login(User user) {
        User Ruser = null;
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from user where `userid`='" + user.getUserID() + "'");
        ResultSet rs = dbconn.getRs();
        if (rs != null) {
            try {
                if (rs.next()) {
                    try {
                        if (rs.getString("userpwd").equals(user.getUserPwd())) {
                            Ruser = user;
                            Ruser.setId(rs.getInt("id"));
                            Ruser.setUserName(rs.getString("username"));
                            Ruser.setUserClassID(rs.getInt("classid"));
                            Ruser.setUserPhone(rs.getString("userphone"));
                            Ruser.setUserRegtime(rs.getString("regtime"));
                            Ruser.setUserType(rs.getInt("usertype"));
                            Ruser.setUserBalance(rs.getInt("balance"));
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        dbconn.closeALL();
        return Ruser;
    }

    @Override
    public User GetUserInfo(User user) {
        User Ruser = null;
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from user where `userid`='" + user.getUserID() + "'");
        ResultSet rs = dbconn.getRs();
        try {
            if (rs.next()) {
                try {
                    Ruser = user;
                    Ruser.setId(rs.getInt("id"));
                    Ruser.setUserID(rs.getInt("userid"));
                    Ruser.setUserPwd(rs.getString("userpwd"));
                    Ruser.setUserName(rs.getString("username"));
                    Ruser.setUserClassID(rs.getInt("classid"));
                    Ruser.setUserPhone(rs.getString("userphone"));
                    Ruser.setUserRegtime(rs.getString("regtime"));
                    Ruser.setUserType(rs.getInt("usertype"));
                    Ruser.setUserBalance(rs.getInt("balance"));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        dbconn.closeALL();
        return Ruser;
    }

    @Override
    public boolean CheckLogin(User user) {
        return false;
    }

    @Override
    public boolean ChangeInfo(User user, String field, String newdata) {
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        boolean upres = conn
                .update("UPDATE user SET `" + field + "`='" + newdata + "' where `userid`='" + user.getUserID() + "'");
        conn.closeALL();
        return upres;
    }

    @Override
    public boolean register(User user) {
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from user where `userid`='" + user.getUserID() + "'");
        ResultSet rs = dbconn.getRs();
        try {
//        	判断是否存在重复用户信息
            if (rs.next()) {
                dbconn.closeALL();
                return false;
            } else {
                String sql = "INSERT INTO user(`userid`,`username`,`userpwd`,`usertype`,`regtime`,`userphone`,`balance`,`classid`)VALUES('"
                        + user.getUserID() + "'," + "'" + user.getUserName() + "','" + user.getUserPwd() + "','"
                        + user.getUserType() + "','" + user.getUserRegtime() + "','" + user.getUserPhone() + "'," + "'"
                        + user.getUserBalance() + "','" + user.getUserClassID() + "')";
                boolean insertres = dbconn.update(sql);
                dbconn.closeALL();
                return insertres;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean DelUser(User user) {
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        boolean delres = conn.update("DELETE from user where `userid`='" + user.getUserID() + "'");
        conn.closeALL();
        return delres;
    }

    @Override
    public List<User> GetUserList(int startnum, int limit) {
        List<User> list = new ArrayList<>();
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        String sql = "select * from user limit " + startnum + "," + limit + "";
        conn.query(sql);
        ResultSet rs = conn.getRs();
        try {
            while (rs.next()) {
                User tuser = new User();
                tuser.setId(rs.getInt("id"));
                tuser.setUserID(rs.getInt("userid"));
                tuser.setUserPwd(rs.getString("userpwd"));
                tuser.setUserName(rs.getString("username"));
                tuser.setUserClassID(rs.getInt("classid"));
                tuser.setUserPhone(rs.getString("userphone"));
                tuser.setUserRegtime(rs.getString("regtime"));
                tuser.setUserType(rs.getInt("usertype"));
                tuser.setUserBalance(rs.getInt("balance"));
                list.add(tuser);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        conn.closeALL();
        return list;
    }

    @Override
    public List<User> GetUserListByClassid(int classid) {
        List<User> list = new ArrayList<>();
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        conn.query("select * from user where `classid`='" + classid + "'");
        ResultSet rs = conn.getRs();
        if (rs != null) {
            while (true) {
                try {
                    if (!rs.next())
                        break;
                    User tuser = new User();
                    tuser.setId(rs.getInt("id"));
                    tuser.setUserID(rs.getInt("userid"));
                    tuser.setUserPwd(rs.getString("userpwd"));
                    tuser.setUserName(rs.getString("username"));
                    tuser.setUserClassID(rs.getInt("classid"));
                    tuser.setUserPhone(rs.getString("userphone"));
                    tuser.setUserRegtime(rs.getString("regtime"));
                    tuser.setUserType(rs.getInt("usertype"));
                    tuser.setUserBalance(rs.getInt("balance"));
                    list.add(tuser);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        conn.closeALL();
        return list;
    }

    @Override
    public List<User> SearchList(String type, String value) {
        List<User> list = new ArrayList<>();
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        conn.query("select * from user where `" + type + "` like '%" + value + "%'");
        ResultSet rs = conn.getRs();
        if (rs != null) {
            while (true) {
                try {
                    if (!rs.next())
                        break;
                    User tuser = new User();
                    tuser.setId(rs.getInt("id"));
                    tuser.setUserID(rs.getInt("userid"));
                    tuser.setUserPwd(rs.getString("userpwd"));
                    tuser.setUserName(rs.getString("username"));
                    tuser.setUserClassID(rs.getInt("classid"));
                    tuser.setUserPhone(rs.getString("userphone"));
                    tuser.setUserRegtime(rs.getString("regtime"));
                    tuser.setUserType(rs.getInt("usertype"));
                    tuser.setUserBalance(rs.getInt("balance"));
                    list.add(tuser);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        conn.closeALL();
        return list;
    }
}
