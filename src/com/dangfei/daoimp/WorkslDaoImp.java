package com.dangfei.daoimp;

import com.dangfei.dao.WorksDao;
import com.dangfei.dbconnect.Dbconnect;
import com.dangfei.model.Works;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkslDaoImp implements WorksDao {

    @Override
    public Works CreateWork(Works works) {
        Date nowtime = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        works.setCtime(sf.format(nowtime));
        return works;
    }

    @Override
    public boolean InsertToDB(Works works) {
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        String sql = "INSERT INTO works(`title`,`content`,`toclassid`,`price`,`ctime`)VALUES('" + works.getTitle()
                + "','" + works.getContent() + "','" + works.getToclassid() + "','" + works.getPrice() + "','"
                + works.getCtime() + "')";
        boolean res = conn.update(sql);
        conn.closeStm();
        conn.closeConn();
        return res;
    }

    @Override
    public List<Works> GetDetailByClassid(int classid) {
        List<Works> res = new ArrayList<>();
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        String sql = "select * from works where `toclassid`='" + classid + "'";
        conn.query(sql);
        ResultSet rs = conn.getRs();
        if (rs != null) {
            while (true) {
                try {
                    if (!rs.next())
                        break;
                    Works tres = new Works();
                    tres.setId(rs.getInt("id"));
                    tres.setTitle(rs.getString("title"));
                    tres.setContent(rs.getString("content"));
                    tres.setToclassid(rs.getInt("toclassid"));
                    tres.setPrice(rs.getDouble("price"));
                    tres.setCtime(rs.getString("ctime"));
                    res.add(tres);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return res;
    }

    public List<Works> GetDetailList() {
        List<Works> res = new ArrayList<>();
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        String sql = "select * from works";
        conn.query(sql);
        ResultSet rs = conn.getRs();
        if (rs != null) {
            while (true) {
                try {
                    if (!rs.next())
                        break;
                    Works tres = new Works();
                    tres.setId(rs.getInt("id"));
                    tres.setTitle(rs.getString("title"));
                    tres.setContent(rs.getString("content"));
                    tres.setToclassid(rs.getInt("toclassid"));
                    tres.setPrice(rs.getDouble("price"));
                    tres.setCtime(rs.getString("ctime"));
                    res.add(tres);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return res;
    }

    @Override
    public Works GetInfoByID(int id) {
        Works res = null;
        Dbconnect conn = new Dbconnect();
        conn.createConn();
        String sql = "select * from works where `id`='" + id + "'";
        conn.query(sql);
        ResultSet rs = conn.getRs();
        if (rs != null) {
            while (true) {
                try {
                    if (!rs.next())
                        break;
                    res = new Works();
                    res.setId(rs.getInt("id"));
                    res.setTitle(rs.getString("title"));
                    res.setContent(rs.getString("content"));
                    res.setToclassid(rs.getInt("toclassid"));
                    res.setPrice(rs.getDouble("price"));
                    res.setCtime(rs.getString("ctime"));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return res;
    }
}
