package com.dangfei.daoimp;

import com.dangfei.dao.DetailDao;
import com.dangfei.dbconnect.Dbconnect;
import com.dangfei.model.Detail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

public class DetailDaoImp implements DetailDao {

    @Override
    public Detail CreateOne(Detail detail) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat sfy = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date nowtime = new Date();
        String detailid = sf.format(nowtime);
        detail.setCreateTime(sfy.format(nowtime));
        return detail;
    }

    @Override
    public List<Detail> GetDetailByToUser(String userid, int Status) {
        return null;
    }

    @Override
    public boolean ChangeInfo(Detail detail) {
        return false;
    }

    @Override
    public Detail GetDetailByClassidAndUserid(int userid, int workid) {
        Detail res = null;
        Dbconnect db = new Dbconnect();
        db.createConn();
        String sql = "select * from detail where `userid`='" + userid + "' and `workid`='" + workid + "'";
        db.query(sql);
        ResultSet rs = db.getRs();
        if (rs != null) {
            try {
                if (rs.next()) {
                    res = new Detail();
                    res.setinfo(rs);
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        db.closeALL();
        return res;
    }

    @Override
    public boolean DetailComplete(Detail detail) {
        boolean res = false;
        Dbconnect db = new Dbconnect();
        db.createConn();
        String check = "select * from detail where `userid`='" + detail.getUserId() + "'and`workid`='" + detail.getWorkid() + "'";
        db.query(check);
        try {
            if (db.getRs().next()) {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String sql = "INSERT INTO detail(`workid`,`userid`,`createtime`,`status`)VALUES('" + detail.getWorkid() + "','" +
                detail.getUserId() + "','" + detail.getCreateTime() + "','" + detail.getStatus() + "')";
        res = db.update(sql);
        db.closeALL();
        return res;
    }

}
