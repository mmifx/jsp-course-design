package com.dangfei.daoimp;

import com.dangfei.dao.ClassDao;
import com.dangfei.dbconnect.Dbconnect;
import com.dangfei.model.Class;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClassDaoimp implements ClassDao {
    @Override
    public boolean CreateOne(Class newclass) {
        Boolean res = false;
        int maxid = 0;
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from class where `classname`='" + newclass.getClassName() + "'");
        ResultSet rs = dbconn.getRs();
        try {
            // 判断是否存在重复学院名称
            if (rs.next()) {
                dbconn.closeALL();
                return false;
            }
            dbconn.query("SELECT MAX(classid) AS maxid FROM class");
            rs = dbconn.getRs();
            if (rs.next()) {
                maxid = rs.getInt("maxid") + 1;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (maxid == 0) {
            return false;
        }
        String sql = "INSERT INTO class(`classid`,`classname`)VALUES('" + maxid + "','" + newclass.getClassName()
                + "')";
        res = dbconn.update(sql);
        dbconn.closeALL();
        return res;
    }

    @Override
    public List<Class> GetClassList() {
        List<Class> list = new ArrayList<>();
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from class");
        ResultSet rs = dbconn.getRs();
        try {
            while (rs.next()) {
                Class tclass = new Class();
                tclass.setID(rs.getInt("id"));
                tclass.setClassID(rs.getInt("classid"));
                tclass.setClassName(rs.getString("classname"));
                dbconn.query("SELECT COUNT(*) AS count FROM `user` WHERE classid = '" + rs.getInt("id") + "'");
                ResultSet countRs = dbconn.getRs();
                if (countRs.next()) {
                    tclass.setAllnumbers(countRs.getInt("count"));
                } else {
                    tclass.setAllnumbers(0);
                }
                list.add(tclass);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;

    }

    @Override
    public String GetClassNameByID(int classid) {
        String res = "暂无信息";
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        String sql = "select * from class where `classid`='" + classid + "'";
        dbconn.query(sql);
        ResultSet rs = dbconn.getRs();
        try {
            if (rs.next()) {
                res = rs.getString("classname");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        dbconn.closeALL();
        return res;

    }

    @Override
    public boolean DelClass(Class delclass) {
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        boolean res = dbconn.update("delete from class where `classid` ='" + delclass.getClassID() + "'");
        dbconn.closeALL();
        return res;
    }

    @Override
    public List<Class> SearchList(String classname) {
        List<Class> list = new ArrayList<>();
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        dbconn.query("select * from class where `classname` like '%" + classname + "%'");
        ResultSet rs = dbconn.getRs();
        try {
            while (rs.next()) {
                Class tclass = new Class();
                tclass.setID(rs.getInt("id"));
                tclass.setClassID(rs.getInt("classid"));
                tclass.setClassName(rs.getString("classname"));
                dbconn.query("SELECT COUNT(*) AS count FROM `user` WHERE classid = '" + rs.getInt("id") + "'");
                ResultSet countRs = dbconn.getRs();
                if (countRs.next()) {
                    tclass.setAllnumbers(countRs.getInt("count"));
                } else {
                    tclass.setAllnumbers(0);
                }
                list.add(tclass);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean UpClassName(Class upclass) {
        Dbconnect dbconn = new Dbconnect();
        dbconn.createConn();
        boolean res = dbconn.update("UPDATE class SET `classname` ='" + upclass.getClassName() + "' WHERE `classid`='" + upclass.getClassID() + "'; ");
        dbconn.closeALL();
        return res;
    }
}
