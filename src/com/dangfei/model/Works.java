package com.dangfei.model;

public class Works {
    private int id;
    private String title;
    private String content;
    private int toclassid;
    private double price;
    private String Ctime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getToclassid() {
        return toclassid;
    }

    public void setToclassid(int toclassid) {
        this.toclassid = toclassid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCtime() {
        return Ctime;
    }

    public void setCtime(String ctime) {
        Ctime = ctime;
    }
}
