package com.dangfei.model;

public class User {
	private int id;
	private String UserName;
	private String UserPwd;
	private int UserID;
	private int UserType;
	private String UserRegtime;
	private String UserPhone;
	private int UserBalance;
	private int UserClassID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUserPwd() {
		return UserPwd;
	}

	public void setUserPwd(String userPwd) {
		UserPwd = userPwd;
	}

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public String getUserRegtime() {
		return UserRegtime;
	}

	public void setUserRegtime(String userRegtime) {
		UserRegtime = userRegtime;
	}

	public String getUserPhone() {
		return UserPhone;
	}

	public void setUserPhone(String userPhone) {
		UserPhone = userPhone;
	}

	public int getUserBalance() {
		return UserBalance;
	}

	public void setUserBalance(int userBalance) {
		UserBalance = userBalance;
	}

	public int getUserClassID() {
		return UserClassID;
	}

	public void setUserClassID(int userClassID) {
		UserClassID = userClassID;
	}
}
