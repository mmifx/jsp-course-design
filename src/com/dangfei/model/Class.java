package com.dangfei.model;

public class Class {
    private String ClassName;
    private int ClassID;
    private int ID;

    public int getAllnumbers() {
        return Allnumbers;
    }

    public void setAllnumbers(int allnumbers) {
        Allnumbers = allnumbers;
    }
    private int Allnumbers;
    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
