package com.dangfei.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Detail {
    private int ID;
    private String UserId;
    private int workid;
    private String CreateTime;
    private int Status;

    public int getWorkid() {
        return workid;
    }

    public void setWorkid(int workid) {
        this.workid = workid;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(String createTime) {
        CreateTime = createTime;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public void setinfo(ResultSet rs) {
        try {
            this.setID(rs.getInt("id"));
            this.setCreateTime(rs.getString("createtime"));
            this.setStatus(rs.getInt("status"));
            this.setWorkid(rs.getInt("workid"));
            this.setUserId(rs.getString("userid"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
