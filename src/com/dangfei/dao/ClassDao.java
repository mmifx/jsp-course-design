package com.dangfei.dao;

import com.dangfei.model.Class;

import java.util.List;

public interface ClassDao {
    /**
     * 新增一个学院
     *
     * @param newclass
     * @return boolean
     */
    boolean CreateOne(Class newclass);

    /**
     * 获取所有学院名称
     *
     * @return array
     */
    List<Class> GetClassList();

    /**
     * 获取指定的学院名称
     *
     * @param classid
     * @return String
     */
    String GetClassNameByID(int classid);

    /**
     * 删除指定的学院
     *
     * @param delclass
     * @return boolean
     */
    boolean DelClass(Class delclass);

    /**
     * 模糊搜索 搜索指定列表信息
     *
     * @return list
     */
    List<Class> SearchList(String classname);

    /**
     * 更新学院名称
     *
     * @return boolean
     */
    boolean UpClassName(Class upclass);
}
