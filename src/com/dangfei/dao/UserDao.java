package com.dangfei.dao;

import com.dangfei.model.User;

import java.util.List;

public interface UserDao {
	/**
	 * 用户登录
	 *
	 * @param user
	 * @return User
	 */
	User login(User user);

	/**
	 * 获取用户信息，根据用户ID
	 *
	 * @param user
	 * @return
	 */

	User GetUserInfo(User user);

	/**
	 * 检查用户登录状态
	 *
	 * @param user
	 * @return boolean
	 */
	boolean CheckLogin(User user);

	/**
	 * 用户修改个人信息 传入新用户信息，根据id更新信息，然后根据field判断要更新的字段
	 *
	 * @param user,field
	 * @return
	 */
	boolean ChangeInfo(User user, String field, String newdata);

	/**
	 * 注册
	 *
	 * @param user
	 * @return
	 */
	boolean register(User user);

	/**
	 * 删除
	 *
	 * @param user
	 * @return
	 */
	boolean DelUser(User user);

	/**
	 * 获取用户列表
	 *
	 * @return
	 */
	List<User> GetUserList(int startnum, int limit);

	/**
	 * 获取指定学院的学生列表
	 *
	 * @param classid
	 * @return
	 */
	List<User> GetUserListByClassid(int classid);


	/**
	 * 根据搜搜类型和关键字来获取用户列表
	 * @param type
	 * @param value
	 * @return
	 */
	List<User> SearchList(String type,String value);

}
