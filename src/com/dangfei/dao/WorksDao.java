package com.dangfei.dao;

import com.dangfei.model.Detail;
import com.dangfei.model.Works;

import java.util.List;

public interface WorksDao {
    /**
     * 新建一条任务 传入参数为
     *
     * @param works
     * @return
     */
    Works CreateWork(Works works);

    /**
     * 将当前work存入到数据库
     *
     * @param works
     * @return
     */
    boolean InsertToDB(Works works);

    /**
     * 根据要发送到的学院ID来获取用户所有要执行的数组
     *
     * @param classid
     * @return
     */
    List<Works> GetDetailByClassid(int classid);

    /**
     * 获取所有任务列表
     * @return
     */
    List<Works> GetDetailList();

    /**
     * 根据任务ID获取任务信息
     * @param id
     * @return
     */
    Works GetInfoByID(int id);
}
