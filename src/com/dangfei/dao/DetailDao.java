package com.dangfei.dao;

import com.dangfei.model.Detail;

import java.util.*;

public interface DetailDao {
    /**
     * 新建一个订单
     *
     * @param detail
     * @return
     */
    Detail CreateOne(Detail detail);

    /**
     * 根据用户id和状态条件查询当前用户所有订单
     * 1：待完成 2：已完成
     *
     * @param userid,Status
     * @return
     */
    List<Detail> GetDetailByToUser(String userid, int Status);

    /**
     * 更新订单信息
     *
     * @param detail
     * @return
     */
    boolean ChangeInfo(Detail detail);

    /**
     * 根据要发送的学院id和用户名来获取订单列表
     *
     * @param userid,workid
     * @return detail/null
     */
    Detail GetDetailByClassidAndUserid(int userid, int workid);

    /***
     * 用户付款成功！
     * @param detail
     * @return
     */
    boolean DetailComplete(Detail detail);
}
