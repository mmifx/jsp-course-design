package com.dangfei.servlet;

import com.dangfei.dao.UserDao;
import com.dangfei.daoimp.UserDaoimp;
import com.dangfei.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class Register
 */
@WebServlet("/ChangeUserInfo")
public class ChangeUserInfo extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeUserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        request.setCharacterEncoding("utf-8");
        String username = request.getParameter("username");
        String password = request.getParameter("userpwd");
        // 设置编码输出格式
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String res = "{\"code\":201,\"msg\":\"参数缺失\"}";
//		判断用户名和密码是否均不为空
        if ((username == null) || (password == null)) {
        } else if (session.getAttribute("userid") == null) {

        } else {
            boolean upres;
            int userid = Integer.parseInt(session.getAttribute("userid").toString());
            //判断是不是仅修改用户名
            if (password.equals("")) {
                User user = new User();
                user.setUserID(userid);
                UserDao userdao = new UserDaoimp();
                upres = userdao.ChangeInfo(user, "username", username);
            }
            //判断是不是仅修改密码
            else if (username.equals("")) {
                User user = new User();
                user.setUserID(userid);
                UserDao userdao = new UserDaoimp();
                upres = userdao.ChangeInfo(user, "userpwd", password);
            }
            //两个都修改
            else {
                User user = new User();
                user.setUserID(userid);
                UserDao userdao = new UserDaoimp();
                boolean upres1 = userdao.ChangeInfo(user, "userpwd", password);
                boolean upres2 = userdao.ChangeInfo(user, "username", username);
                upres = upres1 && upres2;
            }
            if (upres) {
                res = "{\"code\":200,\"msg\":\"修改成功\"}";
            } else {
                res = "{\"code\":202,\"msg\":\"修改失败\"}";
            }
        }
        out.println(res);
    }

}
