package com.dangfei.servlet;

import com.dangfei.dao.UserDao;
import com.dangfei.daoimp.UserDaoimp;
import com.dangfei.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class Register
 */
@WebServlet("/UpUserInfo")
public class UpUserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpUserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String userid = request.getParameter("userid");
		String username = request.getParameter("username");
		String password = request.getParameter("userpwd");
		String userclass = request.getParameter("classid");
		String userbalance = request.getParameter("balance");
		// 设置编码输出格式
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		String res = "{\"code\":201,\"msg\":\"参数缺失\"}";
		// 判断用户名和密码是否均不为空
		if ((username == null) || (userid == null) || (password == null) || (userclass == null)
				|| (userbalance == null)) {
		} else {
			boolean upres = false;
			// 判断是不是仅修改用户名
			UserDao UD = new UserDaoimp();
			User olduser = new User();
			olduser.setUserID(Integer.parseInt(userid));
			olduser = UD.GetUserInfo(olduser);
//            更新 UserName
			if (!(olduser.getUserName().equals(username))) {
				upres = UD.ChangeInfo(olduser, "username", username);
				System.out.println("更新用户名" + upres);
			}
//        更新userpwd
			if (!(olduser.getUserPwd().equals(password))) {
				upres = UD.ChangeInfo(olduser, "userpwd", password);
				System.out.println("更新密码" + upres);
			}
//                更新归属学院
			if (!userclass.equals("0")) {
				System.out.println("归属学院不为0，需要修改");
				if (olduser.getUserClassID() != Integer.parseInt(userclass)) {
					upres = UD.ChangeInfo(olduser, "classid", userclass);
					System.out.println("更新归属学院" + upres);
				}
			}
//            更新余额
			if (olduser.getUserBalance() != Integer.parseInt(userbalance)) {
				upres = UD.ChangeInfo(olduser, "balance", userbalance);
				System.out.println("更新余额" + upres);
			}
			if (upres) {
				res = "{\"code\":200,\"msg\":\"修改成功\"}";
			} else {
				res = "{\"code\":202,\"msg\":\"修改失败，有可能您没有修改任何东西！\"}";
			}
		}
		out.println(res);
	}
}
