package com.dangfei.servlet;

import com.dangfei.dao.ClassDao;
import com.dangfei.dao.UserDao;
import com.dangfei.daoimp.ClassDaoimp;
import com.dangfei.daoimp.UserDaoimp;
import com.dangfei.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class Register
 */
@WebServlet("/GetUserInfo")
public class GetUserInfo extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        request.setCharacterEncoding("utf-8");
        String username = request.getParameter("userid");
        // 设置编码输出格式
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String res = "{\"code\":201,\"msg\":\"参数缺失\"}";

//		判断用户名和密码是否均不为空
        if (username == null) {
        } else if ((session.getAttribute("userid") == null) || session.getAttribute("isadmin") == null) {

        } else {
            UserDao UD = new UserDaoimp();
            User user = new User();
            user.setUserID(Integer.parseInt(username));
            user = UD.GetUserInfo(user);
            ClassDao CD = new ClassDaoimp();
            String UserClassName = CD.GetClassNameByID(user.getUserClassID());
            res = "{\"code\":200,\"msg\":\"获取信息成功\",\"data\":{\"userid\":" + user.getUserID() + ",\"userpwd\":\"" + user.getUserPwd() + "\",\"username\":\"" + user.getUserName() + "\",\"price\":" + user.getUserBalance() + ",\"classname\":\"" + UserClassName + "\",\"class\":" + user.getUserClassID() + " }}";
        }
        out.println(res);
    }

}
