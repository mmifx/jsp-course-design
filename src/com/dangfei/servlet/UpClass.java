package com.dangfei.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import com.dangfei.dao.ClassDao;
import com.dangfei.daoimp.ClassDaoimp;
import com.dangfei.model.Class;

/**
 * Servlet implementation class Register
 */
@WebServlet("/UpClass")
public class UpClass extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpClass() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String classid = request.getParameter("classid");
        String newname = request.getParameter("newname");
        //设置编码输出格式
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        String res = "{\"code\":201,\"msg\":\"修改失败，参数缺失\"}";
        //判断用户名和密码是否均不为空
        if ((classid == null) || (newname == null)) {
        } else {
            if ((classid.equals("")) || (newname.equals(""))) {

            } else {
                Class upclass = new Class();
                upclass.setClassID(Integer.parseInt(classid));
                upclass.setClassName(newname);
                ClassDao cd = new ClassDaoimp();
                boolean flag = cd.UpClassName(upclass);
                if (flag) {
                    res = "{\"code\":200,\"msg\":\"修改学院名称成功!\"}";
                } else {
                    res = "{\"code\":202,\"msg\":\"修改学院名称失败！\"}";
                }
            }
        }
        out.println(res);
    }

}
