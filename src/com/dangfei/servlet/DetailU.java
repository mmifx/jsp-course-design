package com.dangfei.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dangfei.dao.DetailDao;
import com.dangfei.dao.UserDao;
import com.dangfei.dao.WorksDao;
import com.dangfei.daoimp.DetailDaoImp;
import com.dangfei.daoimp.UserDaoimp;
import com.dangfei.daoimp.WorkslDaoImp;
import com.dangfei.model.Detail;
import com.dangfei.model.User;

/**
 * Servlet implementation class Detail
 */
@WebServlet("/DetailU")
public class DetailU extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailU() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        response.setHeader("Content-Type", "text/json,charset=utf-8");
        String msg = "{\"code\":201,\"msg\":\"参数缺失!\"}";
        if (request.getParameter("id") != null) {
            int detailID = Integer.parseInt(request.getParameter("id"));

            User user = new User();
            UserDao ud = new UserDaoimp();
            user.setUserID(Integer.parseInt(session.getAttribute("userid").toString()));
            user = ud.GetUserInfo(user);
            WorksDao WD = new WorkslDaoImp();

            if (user.getUserBalance() < WD.GetInfoByID(detailID).getPrice()) {
                msg = "{\"code\":202,\"msg\":\"余额不足!\"}";
            } else {
                ud.ChangeInfo(user, "balance", String.valueOf(user.getUserBalance() - WD.GetInfoByID(detailID).getPrice()));
                Detail detail = new Detail();
                DetailDao dd = new DetailDaoImp();
                detail.setWorkid(detailID);
                detail.setUserId(session.getAttribute("userid").toString());
                detail = dd.CreateOne(detail);
                detail.setStatus(1);
                boolean res = dd.DetailComplete(detail);
                if (res) {
                    msg = "{\"code\":200,\"msg\":\"新增成功!\"}";
                }
            }
        }
        out.println(msg);
    }
}
