package com.dangfei.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import com.dangfei.dao.ClassDao;
import com.dangfei.daoimp.ClassDaoimp;
import com.dangfei.model.Class;

/**
 * Servlet implementation class Register
 */
@WebServlet("/AddClass")
public class AddClass extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddClass() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String name = request.getParameter("classname");
        //设置编码输出格式
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        String res = "{\"code\":201,\"msg\":\"参数缺失\"}";
        //判断用户名和密码是否均不为空
        if (name == null) {
        } else {
            if (name.equals("")) {

            } else {
                Class newclass = new Class();
                newclass.setClassName(name);
                ClassDao cd = new ClassDaoimp();
                Boolean flag = cd.CreateOne(newclass);
                //判断登陆结果
                if (flag) {
                    res = "{\"code\":200,\"msg\":\"新增成功!\"}";
                } else {
                    res = "{\"code\":202,\"msg\":\"新增失败！\"}";
                }
            }
        }
        out.println(res);
    }

}
