package com.dangfei.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.dangfei.dao.UserDao;
import com.dangfei.daoimp.UserDaoimp;
import com.dangfei.model.User;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        request.setCharacterEncoding("utf-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean keeplive = (request.getParameter("keeplive") != null);
        //设置编码输出格式
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        String res = "{\"code\":201,\"msg\":\"参数缺失\"}";
//		判断用户名和密码是否均不为空
        if ((username == null) || (password == null)) {
        } else {
            User user = new User();
            user.setUserID(Integer.parseInt(username));
            user.setUserPwd(password);
            UserDao userdao = new UserDaoimp();
            user = userdao.login(user);
            //判断登陆结果
            if (user == null) {
                res = "{\"code\":201,\"msg\":\"账号或密码错误\"}";
            } else {
                HttpSession session = request.getSession();
                //如果设置了保持登陆
                if (keeplive) {
                    session.setAttribute("keepalive", "1");
                } else {
                    session.setAttribute("keepalive", "0");
                }
                //设置用户ID到session
                session.setAttribute("userid", user.getUserID());
//                判断用户类型
                switch (user.getUserType()) {
//                    如果是普通用户
                    case 1:
                        res = "{\"code\":200,\"msg\":\"登录成功\",\"url\":\"user.jsp\"}";
                        break;
                    case 2:
//                        如果是管理员
                        session.setAttribute("isadmin", "1");
                        res = "{\"code\":200,\"msg\":\"登录成功\",\"url\":\"admin.jsp\"}";
                        break;
                    default:
                        res = "{\"code\":200,\"msg\":\"登录成功\",\"url\":\"user.jsp\"}";
                        break;
                }
            }
        }
        out.println(res);
    }

}
