<%@ page import="com.dangfei.dao.ClassDao"%>
<%@ page import="com.dangfei.daoimp.ClassDaoimp"%>
<%@ page import="java.util.List"%>
<%@ page import="com.dangfei.model.Class"%>
<%@ page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<jsp:include page="head.jsp">
	<jsp:param value="注册" name="title" />
	<jsp:param value="login" name="cssname" />
</jsp:include>
<body>
	<div id="loading"
		class="m-0 p-0 d-flex justify-content-center align-items-center"
		style="background-color: rgba(77, 88, 99, 1); opacity: 0.5; height: 100%; width: 100%; z-index: -999; position: fixed;">
		<div class="text-success">
			<div class="spinner-border" style="width: 50px; height: 50px;"
				role="status">
				<span class="sr-only">Loading...</span>
			</div>
		</div>
	</div>
	<div class="bg-img">
		<img src="img/login-bg.jpg" alt="">
	</div>
	<!--这里 放注册 -->
	<div class="container" style="margin-top: 100px;">
		<div class="row justify-content-center">
			<div class="col-4 login-box">
				<h3>注册</h3>
				<form>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">账号:</span>
						</div>
						<input type="number" class="form-control" id="userid"
							name="userid" aria-label="Sizing example input"
							aria-describedby="userid" required placeholder="*不低于5位的数字">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">密码:</span>
						</div>
						<input type="password" class="form-control" id="userpwd"
							name="userpwd" aria-label="Sizing example input"
							aria-describedby="userpwd" required placeholder="*必填">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">姓名:</span>
						</div>
						<input type="text" class="form-control" id="username"
							name="username" aria-label="Sizing example input"
							aria-describedby="username" required placeholder="*请输入您的真实姓名">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">联系方式:</span>
						</div>
						<input type="text" class="form-control" id="userphone"
							name="userphone" aria-label="Sizing example input"
							aria-describedby="userphone" required placeholder="*请输入您的手机号">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="inputGroupSelect01">所属学院</label>
						</div>
						<select class="custom-select" name="classid" required
							id="inputGroupSelect01">
							<option value="0">默认</option>
							<%
							ClassDao cdao = new ClassDaoimp();
							List<Class> clist = cdao.GetClassList();
							if (clist.isEmpty()) {
							%>
							<option value="0">暂无更多选项</option>
							<%
								} else {
							for (int i = 0; i < clist.size(); i++) {
							%>
							<option value="<%=clist.get(i).getClassID()%>"><%=clist.get(i).getClassName()%>
							</option>
							<%
								}
							}
							%>
						</select>
					</div>
					<br>
					<div class="row justify-content-around">
						<button type="submit" class="btn btn-success col-3">注册</button>
						<a href="index.jsp" class="btn btn-secondary col-3">前往登录</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<%@include file="foot.jsp"%>
	<script>
		function CheckForm() {
			var userid = $("#userid").val();
			var username = $("#username").val();
			var userpwd = $("#userpwd").val();
			var userphone = $("#userphone").val();
			var userclass = $("select").val();
			if (userid.length < 5) {
				alert("账号长度不能小于5位")
				return false;
			}
			if (username.length < 2) {
				alert("姓名长度不能低于2位")
				return false
			}
			if (userphone.length != 11) {
				alert("手机号格式不正确")
				return false;
			}
			return true;
		}

		$("form").submit(function() {

			$("#loading").css('z-index', "999");
			if (!CheckForm()) {
				return false;
			}
			var forms = $("form").serialize();
			$.ajax({
				url : "Register",
				type : "post",
				dataType : "json",
				data : forms,
				success : function(res) {
					alert(res.msg)
					if (res.code == 200) {
						window.location.href = res.url;
					}
				},
				complete : function() {
					$("#loading").css('z-index', "-999");
				}
			})
			return false;
		})
	</script>
</body>

</html>