<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ page
        import="com.dangfei.model.User,com.dangfei.dao.UserDao,com.dangfei.daoimp.UserDaoimp" %>
<%
    request.setCharacterEncoding("utf-8");
%>
<jsp:include page="head.jsp">
    <jsp:param value="登录" name="title"/>
    <jsp:param value="login" name="cssname"/>
</jsp:include>
<body>
<div id="loading"
     class="m-0 p-0 d-flex justify-content-center align-items-center"
     style="display: none !important; background-color: rgba(77, 88, 99, 1); opacity: 0.5; height: 100%; width: 100%; position: fixed; z-index: 100;">
    <div class="text-success">
        <div class="spinner-border" style="width: 50px; height: 50px;"
             role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
<div class="bg-img">
    <img src="img/login-bg.jpg" alt="">
</div>
<!-- logo -->
<header>
    <img src="img/login-logo.png" alt="">
</header>
<!--这里 放登录 -->
<div class="container">
    <div class="row justify-content-end">
        <div class="col-4 login-box">
            <h3>登录</h3>
            <form id="loginbox">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="username">账号:</span>
                    </div>
                    <input type="text" class="form-control" name="username"
                           aria-label="Sizing example input" aria-describedby="username"
                           required>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="userpwd">密码:</span>
                    </div>
                    <input type="password" class="form-control" name="password"
                           aria-label="Sizing example input" aria-describedby="userpwd"
                           required>
                </div>
                <!-- 功能区 -->
                <div class="row align-content-between">
                    <div class="custom-control custom-checkbox col">
                        <input type="checkbox" name="keeplive" id="customCheck1"
                               class="custom-control-input"> <label
                            class="custom-control-label" for="customCheck1">记住密码</label>
                    </div>
                    <div class="col">
                        <a href="register.jsp">注册账号</a>
                    </div>
                </div>
                <br>
                <div class="row justify-content-around">
                    <button type="submit" class="btn btn-success col-3">登录</button>
                    <button type="reset" class="btn btn-secondary col-3">清空</button>
                </div>
            </form>
        </div>
    </div>
</div>
<%@ include file="foot.jsp" %>
<script>
    $("form").submit(function (e) {
        $("#loading").css('z-index', "999");
        $.ajax({
            url: "Login",
            type: "POST",
            data: $("#loginbox").serialize(),
            dataType: "json",
            success: function (e) {
                if (e.code != 200) {
                    alert(e.msg);
                } else {
                    alert(e.msg);
                    console.log(e.url);
                    window.location.href = e.url;
                }
            },
            complete: function (e) {
                $("#loading").css('z-index', "-999");
            }
        })
        return false;
    })
</script>
</body>

</html>