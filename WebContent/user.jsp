<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ page
        import="com.dangfei.model.User,com.dangfei.dao.UserDao,com.dangfei.daoimp.UserDaoimp" %>
<%@ page import="com.dangfei.dao.ClassDao" %>
<%@ page import="com.dangfei.daoimp.ClassDaoimp" %>
<%
    String userid = (session.getAttribute("userid") == null) ? "null" : session.getAttribute("userid").toString();
    if (userid == "null") {
%>
<jsp:forward page="index.jsp"/>
<%
    }
    User user = new User();
    UserDao userDao = new UserDaoimp();
    user.setUserID(Integer.parseInt(userid));
    user = userDao.GetUserInfo(user);
    ClassDao cdao = new ClassDaoimp();
%>
<%
    request.setCharacterEncoding("utf-8");
%>
<jsp:include page="head.jsp">
    <jsp:param value="用户中心" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>

<body>
<div id="loading"
     class="m-0 p-0 d-flex justify-content-center align-items-center"
     style="display: none!important; background-color: rgba(77, 88, 99, 1); opacity: 0.5; height: 100%; width: 100%; position: fixed; z-index: 100;">
    <div class="text-success">
        <div class="spinner-border" style="width: 50px; height: 50px;"
             role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item active"><a href="user.jsp"
                                                  class="text-white">个人信息</a></li>
            <li class="list-group-item"><a href="detail.jsp"
                                           class="text-dark">订单管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <div class="col-10 col-md-10 m-0 p-0">
        <div class="row userimg">
            <img src="img/user.svg" alt="">
        </div>
        <div class="row col-12 col-md-9 offset-md-2 mt-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">账号</span>
                </div>
                <input type="text" class="form-control" aria-label="Sizing input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=user.getUserID()%>" readonly>
                <div class="input-group-append">
                    <span class="input-group-text">*不可修改</span>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">账户余额</span>
                </div>
                <input type="text" class="form-control"
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=user.getUserBalance()%>￥" readonly>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">归属学院</span>
                </div>
                <input type="text" class="form-control"
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=cdao.GetClassNameByID(user.getUserClassID())%>" readonly>
            </div>
            <form class="col-12 m-0 p-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">姓名</span>
                    </div>
                    <input type="text" class="form-control" name="username"
                           aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default"
                           placeholder="<%=user.getUserName()%>">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">密码</span>
                    </div>
                    <input type="password" class="form-control" name="userpwd"
                           aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default" placeholder="******">
                </div>
                <div class="w-100" style="text-align: right;">
                    <button class="btn btn-success">修改</button>
                </div>
            </form>
        </div>
    </div>
</main>
<%@ include file="foot.jsp" %>
<script>
    $("form").submit(function (e) {
        $("#loading").css('display', "flex!important");
        var forms = $("form").serialize();
        $.ajax({
            url: "ChangeUserInfo",
            type: "POST",
            dataType: "json",
            data: forms,
            success: function (res) {
                alert(res.msg);
            },
            complete: function () {
                $("#loading").css('display', "none!important");
            }
        })
        return false;
    })
</script>
</body>

</html>