<%@ page import="java.util.List" %>
<%@ page import="com.dangfei.model.Class" %>
<%@ page import="com.dangfei.dao.ClassDao" %>
<%@ page import="com.dangfei.daoimp.ClassDaoimp" %>
<%@ page import="com.dangfei.dao.WorksDao" %>
<%@ page import="com.dangfei.daoimp.WorkslDaoImp" %>
<%@ page import="com.dangfei.model.Works" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%
    request.setCharacterEncoding("utf-8");
%>
<%
    if (session.getAttribute("isadmin") == null) {
%>
<jsp:forward page="index.jsp"/>
<%
    }
%>
<jsp:include page="head.jsp">
    <jsp:param value="用户管理" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>
<body>
<div id="loading"
     class="m-0 p-0 d-flex justify-content-center align-items-center"
     style="display: none !important; background-color: rgba(77, 88, 99, 1); opacity: 0.5; height: 100%; width: 100%; position: fixed; z-index: 100;">
    <div class="text-success">
        <div class="spinner-border" style="width: 50px; height: 50px;"
             role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item"><a href="admin.jsp"
                                           class="text-dark">个人信息</a></li>
            <li class="list-group-item active"><a href="admin_pay.jsp"
                                                  class="text-white">缴费管理</a></li>
            <li class="list-group-item"><a href="admin_user.jsp"
                                           class="text-dark">用户管理</a></li>
            <li class="list-group-item"><a href="admin_class.jsp"
                                           class="text-dark">学院管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <!-- 用户列表 -->
    <div class="col-10 col-md-10 m-0 p-0">
        <!-- 未完成的订单 -->
        <div class="row container">
            <p>
                <a class="btn btn-success" data-toggle="collapse" href="#collapseExample" role="button"
                   aria-expanded="false" aria-controls="collapseExample">发起新缴费
                </a>
            </p>
            <br>
        </div>
        <div class="collapse" id="collapseExample">
            <div class="card card-body">
                <form id="search">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">标题</span>
                        </div>
                        <input type="text" class="form-control" name="title" placeholder="订单的标题"/>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">说明</span>
                        </div>
                        <textarea class="form-control" name="desc" placeholder="订单的说明"></textarea>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">金额</span>
                        </div>
                        <input type="text" class="form-control" name="price"
                               aria-label="Amount (to the nearest dollar)">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">所有学院</label>
                        </div>
                        <select class="custom-select" name="classid" required
                                id="inputGroupSelect01">
                            <option value="0">默认</option>
                            <%
                                ClassDao cdao = new ClassDaoimp();
                                List<Class> clist = cdao.GetClassList();
                                if (clist.isEmpty()) {
                            %>
                            <option value="0">暂无更多选项</option>
                            <%
                            } else {
                                for (Class aClass : clist) {
                            %>
                            <option value="<%=aClass.getClassID()%>"><%=aClass.getClassName()%>
                            </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">发布</button>
                    <button type="reset" class="btn btn-info">重置</button>
                </form>
            </div>
        </div>
        <div class="alert alert-primary" role="alert">任务列表</div>
        <div class="list-group">
            <%
                WorksDao WD = new WorkslDaoImp();
                List<Works> wdlist = WD.GetDetailList();
                for (int i = 0; i < wdlist.size(); i++) {
            %>
            <a href="#" class="list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1"><%=wdlist.get(i).getTitle()%>
                    </h5>
                    <small>发布时间<%=wdlist.get(i).getCtime()%>
                    </small>
                </div>
                <div class="d-flex w-100 justify-content-between">
                    <p class="mb-1"><%=wdlist.get(i).getContent() %>
                    </p>
                    <small><%=wdlist.get(i).getPrice()%>￥</small>
                </div>
            </a>
            <% } %>
        </div>
    </div>
</main>
<%@ include file="foot.jsp" %>
<script>
    $("form").submit(function (e) {
        var forms = $("form").serialize();
        $("#loading").css('display', "flex!important");
        $.ajax({
            url: "Adddetail",
            type: "POST",
            data: forms,
            DateType: "json",
            success: function (res) {
                res = JSON.parse(res);
                console.log(res);
                alert(res.msg);
            },
            complete: function () {
                $("#loading").css('display', "none!important");
            }
        })
        return false;
    })
</script>
</body>

</html>