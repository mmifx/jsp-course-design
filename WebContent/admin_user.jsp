<%@ page import="com.dangfei.dao.UserDao" %>
<%@ page import="com.dangfei.daoimp.UserDaoimp" %>
<%@ page import="java.util.List" %>
<%@ page import="com.dangfei.model.User" %>
<%@ page import="com.dangfei.model.Class" %>
<%@ page import="com.dangfei.dao.ClassDao" %>
<%@ page import="com.dangfei.daoimp.ClassDaoimp" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%
    request.setCharacterEncoding("utf-8");
%>
<%
    if (session.getAttribute("isadmin") == null) {
%>
<jsp:forward page="index.jsp"/>
<%
    }
%>
<jsp:include page="head.jsp">
    <jsp:param value="用户管理" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>

<%
    boolean tip = false;
    String tips = "";
    UserDao udao = new UserDaoimp();
    List<User> list = null;
    String Type = (request.getParameter("stype") == null) ? null : request.getParameter("stype");
    if (Type == null) {
        list = udao.GetUserList(0, 10);
    } else {
        String keyword = request.getParameter("keyword");
        if (keyword == null) {
            list = udao.GetUserList(0, 10);
        } else {
            list = udao.SearchList(Type, keyword);
            if (list.size() == 0) {
                tip = true;
                tips = "没有符合条件的用户";
            }
        }
    }
%>
<body>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item"><a href="admin.jsp"
                                           class="text-dark">个人信息</a></li>
            <li class="list-group-item"><a href="admin_pay.jsp"
                                           class="text-dark">缴费管理</a></li>
            <li class="list-group-item active"><a href="admin_user.jsp"
                                                  class="text-white">用户管理</a></li>
            <li class="list-group-item"><a href="admin_class.jsp"
                                           class="text-dark">学院管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <!-- 用户列表 -->
    <div class="col-10 col-md-10">
        <!-- 功能区 -->
        <div class="mb-2 row">
            <div class="col-2">
                <a href="adduser.jsp" target="_blank" type="button"
                   class="btn btn-success">新增用户</a>
            </div>
            <div class="col-10">
                <form method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">搜索:</span>
                        </div>
                        <select class="form-control col-2 mx-1" name="stype">
                            <option value="userid">账号</option>
                            <option value="username">姓名</option>
                        </select> <input type="text" name="keyword" class="form-control col-5"
                                         aria-label="Sizing example input"
                                         aria-describedby="inputGroup-sizing-default">
                        <button type="submit" class="btn btn-info mx-2">搜索</button>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">编号</th>
                <th scope="col">账号</th>
                <th scope="col">姓名</th>
                <th scope="col">归属学院</th>
                <th scope="col">账户余额</th>
                <th scope="col">功能</th>
            </tr>
            </thead>
            <tbody>
            <%
                if (tip) {
            %>
            <div class="alert alert-danger" role="alert">
                <%=tips %>
            </div>
            <%
                }
            %>
            <%
                for (int i = 0; i < list.size(); i++) {
                    User tuser = list.get(i);
                    ClassDao classdao = new ClassDaoimp();
            %>
            <tr>
                <th scope="row"><%=i + 1%>
                </th>
                <td><%=tuser.getUserID()%>
                </td>
                <td><%=tuser.getUserName()%>
                </td>
                <td><%=classdao.GetClassNameByID(tuser.getUserClassID())%>
                </td>
                <td><%=tuser.getUserBalance()%>
                </td>
                <td>
                    <button onclick="OpenTop(<%=tuser.getUserID()%>)" class="btn btn-success">修改
                    </button>
                    <button onclick="Delete(<%=tuser.getUserID()%>)"
                            class="btn btn-secondary">删除
                    </button>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</main>
<div class="opentop">
    <div class="container">
        <div class="row title">
            <h5 class="col-12">用户信息修改</h5>
        </div>
        <div class="row justify-content-center">
            <div class="col-6 login-box">
                <form id="upuserinfo">
                    <input type="text" class="form-control" style="display:none;" id="userid" name="userid"
                           aria-label="Sizing example input" aria-describedby="userid" required
                           placeholder="">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">姓名: </span>
                        </div>
                        <input type="text" class="form-control" id="username" name="username"
                               aria-label="Sizing example input" aria-describedby="username" required
                               placeholder="请输入修改后的姓名">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">密码:</span>
                        </div>
                        <input type="text" class="form-control" id="userpwd" name="userpwd"
                               aria-label="Sizing example input" aria-describedby="userpwd" required
                               placeholder="请输入修改后的密码">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">余额:</span>
                        </div>
                        <input type="text" class="form-control" id="balance" name="balance"
                               aria-label="Sizing example input" aria-describedby="balance" required
                               placeholder="请输入修改后的余额">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">所属学院</label>
                        </div>
                        <select class="custom-select" name="classid" id="classselect" required>
                            <option value="0">暂不修改</option>
                            <%
                                ClassDao cdao = new ClassDaoimp();
                                List<Class> clist = cdao.GetClassList();
                                if (clist.isEmpty()) {
                            %>
                            <option value="0">暂无更多选项</option>
                            <%
                            } else {
                                for (Class aClass : clist) {
                            %>
                            <option value="<%=aClass.getClassID()%>"><%=aClass.getClassName()%>
                            </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>
                    <br>
                    <div class="row justify-content-around">
                        <button type="submit" class="btn btn-success col-3">注册</button>
                        <a id="cancel" class="btn btn-info col-3">取消</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@ include file="foot.jsp" %>
<script>
    /**
     * 获取用户信息。
     * 渲染到html
     * 显示opentop
     * 取消时 隐藏opentop
     * 修改提交： 获取userid等信息，发送ajax信息
     * 返回 修改结果，然后隐藏opentop
     */
    var opentop = $(".opentop")[0];

    /**
     * 获取用户信息并渲染到html 然后显示opentop
     *
     */
    function OpenTop(userid) {
//        获取信息
        $.ajax({
            url: "GetUserInfo",
            type: "POST",
            data: {
                userid: userid
            },
            DataType: "json",
            success: function (res) {
                res = JSON.parse(res);
                if (res.code == 200) {
                    $("#userid").val(res.data.userid);
                    $("#username").val(res.data.username);
                    $("#userpwd").val(res.data.userpwd);
                    $("#balance").val(res.data.price);
                    showtop();
                }
                //渲染到html
            },
            complete: function () {
            }
        })
        return false;
    }

    $("#upuserinfo").submit(function () {
        var info = $("#upuserinfo").serialize();
        return false;
    })
    $("#cancel").click(function () {
        hidetop()
    })

    //隐藏弹出层
    function hidetop() {
        opentop.style.display = "none";
    }

    $("#upuserinfo").submit(function () {
            var userinfo = $("#upuserinfo").serialize()
            $.ajax({
                url: "UpUserInfo",
                type: "POST",
                data: userinfo,
                DataType: "json",
                success: function (res) {
                    res = JSON.parse(res);
                    alert(res.msg);
                },
                complete: function () {
                }
            })
            return false;
        }
    )

    //    显示弹出层
    function showtop() {
        opentop.style.display = "block";
    }

    function Delete(userid) {
        alert(userid)
    }
</script>
</body>

</html>