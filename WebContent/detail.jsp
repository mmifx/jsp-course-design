<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ page import="com.dangfei.dao.WorksDao" %>
<%@ page import="com.dangfei.daoimp.WorkslDaoImp" %>
<%@ page import="com.dangfei.model.Works" %>
<%@ page import="java.util.List" %>
<%@ page import="com.dangfei.dao.UserDao" %>
<%@ page import="com.dangfei.daoimp.UserDaoimp" %>
<%@ page import="com.dangfei.model.User" %>
<%@ page import="com.dangfei.model.Detail" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.dangfei.dao.DetailDao" %>
<%@ page import="com.dangfei.daoimp.DetailDaoImp" %>
<%@ page import="java.util.Objects" %>
<%
    String userid = (session.getAttribute("userid") == null) ? "null" : session.getAttribute("userid").toString();
    if (userid.equals("null")) {
%>
<jsp:forward page="index.jsp"/>
<%
    }
%>
<%
    request.setCharacterEncoding("utf-8");
%>
<jsp:include page="head.jsp">
    <jsp:param value="订单管理" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>

<body>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item"><a href="user.jsp"
                                           class="text-dark">个人信息</a></li>
            <li class="list-group-item active"><a href="detail.jsp"
                                                  class="text-white">订单管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <div class="col-10 col-md-10 m-0 p-0">
        <!-- 未完成的订单 -->
        <div>
            <div class="alert alert-danger" role="alert">待处理</div>
            <div class="list-group">
                <%
                    UserDao ud = new UserDaoimp();
                    User user = new User();
                    user.setUserID(Integer.parseInt(userid));
                    user = ud.GetUserInfo(user);
                    WorksDao DD = new WorkslDaoImp();
                    List<Works> dlist = DD.GetDetailByClassid(user.getUserClassID());
                    List<Works> detailing = new ArrayList<Works>();
                    List<Works> detailcomp = new ArrayList<Works>();
                    for (Works works : dlist) {
                        DetailDao ddi = new DetailDaoImp();
                        if (ddi.GetDetailByClassidAndUserid(user.getUserID(), works.getId()) != null) {
                            detailcomp.add(works);
                        } else {
                            detailing.add(works);
                        }
                    }
                    for (Works works : detailing) {
                %>
                <a href="javascript:submit(<%=works.getId() %>)" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><%=works.getTitle()%>
                        </h5>
                        <small>发布时间:<%=works.getCtime()%>
                        </small>
                    </div>
                    <div class="d-flex w-100 justify-content-between">
                        <p class="mb-1"><%=works.getContent()%>
                        </p>
                        <small><%=works.getPrice()%>￥</small>
                    </div>
                </a>
                <%
                    }
                %>
            </div>
        </div>
        <!-- 已完成的历史订单 -->
        <div class="mt-3">
            <div class="alert alert-dark" role="alert">已完成</div>
            <div class="list-group">
                <%
                    for (Works works : detailcomp) {
                %>
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><%=works.getTitle()%>
                        </h5>
                        <small>发布时间:<%=works.getCtime()%>
                        </small>
                    </div>
                    <div class="d-flex w-100 justify-content-between">
                        <p class="mb-1"><%=works.getContent()%>
                        </p>
                        <small><%=works.getPrice()%>￥</small>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</main>
<%@ include file="foot.jsp" %>
<script>
    function submit(id) {
        $.ajax({
            type: "post",
            url: "DetailU",
            data: {id: id},
            dataType: "json",
            success: function (res) {
                if (res.code == 200) {
                    alert(res.msg)
                    location.reload()
                } else {
                    alert(res.msg)
                }
            }
        })
    }
</script>
</body>

</html>