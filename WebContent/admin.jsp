<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ page
        import="com.dangfei.model.User,com.dangfei.dao.UserDao,com.dangfei.daoimp.UserDaoimp" %>
<%@ page import="com.dangfei.daoimp.ClassDaoimp" %>
<%@ page import="com.dangfei.dao.ClassDao" %>
<%
    request.setCharacterEncoding("utf-8");
    String userid = (session.getAttribute("userid") == null) ? "null" : session.getAttribute("userid").toString();
    if ((userid == "null") || (session.getAttribute("isadmin") == null)) {
%>
<jsp:forward page="index.jsp"/>
<%
    }
    User user = new User();
    UserDao userDao = new UserDaoimp();
    user.setUserID(Integer.parseInt(userid));
    user = userDao.GetUserInfo(user);
    ClassDao cdao = new ClassDaoimp();
%>
<jsp:include page="head.jsp">
    <jsp:param value="后台管理" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>
<body>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item active"><a href="admin.jsp"
                                                  class="text-white">个人信息</a></li>
            <li class="list-group-item "><a href="admin_pay.jsp"
                                            class="text-dark">缴费管理</a></li>
            <li class="list-group-item "><a href="admin_user.jsp"
                                            class="text-dark">用户管理</a></li>
            <li class="list-group-item"><a href="admin_class.jsp"
                                           class="text-dark">学院管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <div class="col-10 col-md-10 m-0 p-0">
        <div class="row userimg">
            <img src="img/user.svg" alt="">
        </div>
        <div class="row col-12 col-md-9 offset-md-2 mt-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">账号</span>
                </div>
                <input type="text" class="form-control" aria-label="Sizing input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=user.getUserID()%>" readonly>
                <div class="input-group-append">
                    <span class="input-group-text">*不可修改</span>
                </div>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">账户余额</span>
                </div>
                <input type="text" class="form-control"
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=user.getUserBalance()%>￥" readonly>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">归属学院</span>
                </div>
                <input type="text" class="form-control"
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default"
                       value="<%=cdao.GetClassNameByID(user.getUserClassID())%>" readonly>
            </div>
            <form class="col-12 m-0 p-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">姓名</span>
                    </div>
                    <input type="text" class="form-control" name="username"
                           aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default"
                           placeholder="<%=user.getUserName()%>">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">密码</span>
                    </div>
                    <input type="password" class="form-control" name="userpwd"
                           aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-default" placeholder="******">
                </div>
                <div class="w-100" style="text-align: right;">
                    <button class="btn btn-success">修改</button>
                </div>
            </form>
        </div>
    </div>
</main>
<%@ include file="foot.jsp" %>
<script>
    $("form").submit(function (e) {
        $("#loading").css('z-index', "999");
        $.ajax({
            url: "Login",
            type: "POST",
            data: $("#loginbox").serialize(),
            dataType: "json",
            success: function (e) {
                if (e.code != 200) {
                    alert(e.msg);
                } else {
                    alert(e.msg);
                    console.log(e.url);
                    window.location.href = e.url;
                }
            },
            complete: function (e) {
                $("#loading").css('z-index', "-999");
            }
        })
        return false;
    })
</script>
</body>

</html>