<%@ page import="com.dangfei.dao.UserDao" %>
<%@ page import="com.dangfei.daoimp.UserDaoimp" %>
<%@ page import="java.util.List" %>
<%@ page import="com.dangfei.model.User" %>
<%@ page import="com.dangfei.model.Class" %>
<%@ page import="com.dangfei.dao.ClassDao" %>
<%@ page import="com.dangfei.daoimp.ClassDaoimp" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%
    request.setCharacterEncoding("utf-8");
%>
<%
    if (session.getAttribute("isadmin") == null) {
%>
<jsp:forward page="index.jsp"/>
<%
    }
%>
<jsp:include page="head.jsp">
    <jsp:param value="学院管理" name="title"/>
    <jsp:param value="user" name="cssname"/>
</jsp:include>

<%
    ClassDao cdao = new ClassDaoimp();
    List<Class> list = null;
    String keyword = request.getParameter("keyword");
    if (keyword == null) {
        list = cdao.GetClassList();
    } else {
        list = cdao.SearchList(keyword);
    }
%>
<body>
<header>
    <img src="img/logo.png" alt="">
</header>
<main class="container row">
    <nav class="col-2 col-md-2">
        <ul class="list-group">
            <li class="list-group-item disabled" aria-disabled="true">功能菜单</li>
            <li class="list-group-item"><a href="admin.jsp"
                                           class="text-dark">个人信息</a></li>
            <li class="list-group-item"><a href="admin_pay.jsp"
                                           class="text-dark">缴费管理</a></li>
            <li class="list-group-item"><a href="admin_user.jsp"
                                           class="text-dark">用户管理</a></li>
            <li class="list-group-item active"><a href="admin_class.jsp"
                                                  class="text-white">学院管理</a></li>
            <li class="list-group-item"><a href="Logout" class="text-dark">退出</a></li>
        </ul>
    </nav>
    <!-- 用户列表 -->
    <div class="col-10 col-md-10">
        <!-- 功能区 -->
        <div class="mb-2 row">
            <div class="col-2">
                <a data-toggle="collapse" href="#addnew" type="button"
                   class="btn btn-success">新增学院</a>
            </div>
            <div class="col-10">
                <form id="search" method="POST">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">搜索:</span>
                        </div>
                        <input type="text" name="keyword" class="form-control col-5"
                               aria-label="Sizing example input"
                               aria-describedby="inputGroup-sizing-default">
                        <button type="submit" class="btn btn-info mx-2">搜索</button>
                    </div>
                </form>
            </div>
        </div>
        <div id="addnew" class="collapse row mb-3">
            <form id="add" class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">学院名称:</span>
                    </div>
                    <input type="text" class="form-control" name="classname" aria-label="Sizing example input"
                           aria-describedby="classname" required placeholder="*请输入学院名称">
                </div>
                <button type="submit" class="btn btn-success">新增</button>
                <button type="reset" class="btn btn-info">取消</button>
            </form>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">编号</th>
                <th scope="col">学院名称</th>
                <th scope="col">当前人数</th>
                <th scope="col">功能</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (int i = 0; i < list.size(); i++) {
                    Class tclass = list.get(i);
            %>
            <tr>
                <th scope="row"><%=i + 1%>
                </th>
                <td><%=tclass.getClassName()%>
                </td>
                <td><%=tclass.getAllnumbers()%>
                </td>
                <td>
                    <button class="btn btn-success" data-toggle="collapse"
                            data-target="#collapse<%=tclass.getClassID()%>"
                            aria-expanded="false"
                            aria-controls="collapse<%=tclass.getClassID()%>">修改
                    </button>
                    <a href="javascript:delclass(<%=tclass.getClassID()%>)"
                       class="btn btn-secondary">删除</a>
                </td>
            </tr>
            <div class="collapse" id="collapse<%=tclass.getClassID()%>">
                <div class="card changeform" id="change<%=tclass.getClassID()%>">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">请输入新的学院名称:</span>
                        </div>
                        <input type="text" id="newname<%=tclass.getClassID()%>" class="form-control col-5"
                               aria-label="Sizing example input"
                               aria-describedby="changeclassname"
                               placeholder="<%=tclass.getClassName()%>"/>
                        <a href="javascript:UpClass(<%=tclass.getClassID()%>)" class="btn btn-info mx-2">修改</a>
                    </div>
                </div>
            </div>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</main>
<%@ include file="foot.jsp" %>
<script>
    $("#add").submit(function () {
        var infos = $("#add").serialize();
        $.ajax({
            url: "AddClass",
            method: "POST",
            dataType: "json",
            data: infos,
            success: function (res) {
                alert(res.msg);
            }
        })
        return false;
    })

    function UpClass(classid) {
        const newname = $("#newname" + classid)[0].value;
        $.ajax({
            url: "UpClass",
            method: "POST",
            dataType: "json",
            data: {
                classid: classid,
                newname: newname
            },
            success: function (res) {
                alert(res.msg);
            }
        })
    }

    function delclass(classid) {
        if (confirm("确定删除吗？")) {
            $.ajax({
                url: "DeleteClass",
                method: "POST",
                dataType: "json",
                data: {
                    classid: classid
                },
                success: function (res) {
                    alert(res.msg);
                }
            })
        } else {
        }

    }
</script>
</body>

</html>