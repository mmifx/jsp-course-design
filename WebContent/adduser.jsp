<%@ page import="java.util.List"%>
<%@ page import="com.dangfei.model.Class"%>
<%@ page import="com.dangfei.dao.ClassDao"%>
<%@ page import="com.dangfei.daoimp.ClassDaoimp"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<%
	if (session.getAttribute("isadmin") == null) {
%>
<jsp:forward page="index.jsp" />
<%
	}
%>
<jsp:include page="head.jsp">
	<jsp:param value="用户管理" name="title" />
	<jsp:param value="adduser" name="cssname" />
</jsp:include>
<body>
	<div id="loading"
		class="m-0 p-0 d-flex justify-content-center align-items-center"
		style="display: none !important; background-color: rgba(77, 88, 99, 1); opacity: 0.5; height: 100%; width: 100%; position: fixed; z-index: 100;">
		<div class="text-success">
			<div class="spinner-border" style="width: 50px; height: 50px;"
				role="status">
				<span class="sr-only">Loading...</span>
			</div>
		</div>
	</div>
	<header style="width: 100%; background-color: #7caad4;">
		<img src="img/logo.png" alt="">
	</header>
	<main class="container">
		<div class="row justify-content-center">
			<div class="col-4 login-box">
				<h3>新增用户</h3>
				<form>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">账号:</span>
						</div>
						<input type="number" class="form-control" id="userid"
							name="userid" aria-label="Sizing example input"
							aria-describedby="userid" required placeholder="*不低于5位的数字">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">密码:</span>
						</div>
						<input type="password" class="form-control" id="userpwd"
							name="userpwd" aria-label="Sizing example input"
							aria-describedby="userpwd" required placeholder="*必填">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">姓名: </span>
						</div>
						<input type="text" class="form-control" id="username"
							name="username" aria-label="Sizing example input"
							aria-describedby="username" required placeholder="*请输入真实姓名">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">余额:</span>
						</div>
						<input type="text" class="form-control" id="balance"
							name="balance" aria-label="Sizing example input"
							aria-describedby="balance" required placeholder="*余额">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">联系方式:</span>
						</div>
						<input type="text" class="form-control" id="userphone"
							name="userphone" aria-label="Sizing example input"
							aria-describedby="userphone" required placeholder="*请输入您的手机号">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="inputGroupSelect01">所属学院</label>
						</div>
						<select class="custom-select" name="classid" required
							id="inputGroupSelect01">
							<option value="0">默认</option>
							<%
								ClassDao cdao = new ClassDaoimp();
							List<Class> clist = cdao.GetClassList();
							if (clist.isEmpty()) {
							%>
							<option value="0">暂无更多选项</option>
							<%
								} else {
							for (Class aClass : clist) {
							%>
							<option value="<%=aClass.getClassID()%>"><%=aClass.getClassName()%>
							</option>
							<%
								}
							}
							%>
						</select>
					</div>
					<br>
					<div class="row justify-content-around">
						<button type="submit" class="btn btn-success col-3">注册</button>
						<button type="reset" class="btn btn-secondary col-3">重置</button>
					</div>
				</form>
			</div>
		</div>
	</main>
	<%@ include file="foot.jsp"%>
	<script>
		function CheckForm() {
			var userid = $("#userid").val();
			var username = $("#username").val();
			var userpwd = $("#userpwd").val();
			var userphone = $("#userphone").val();
			var userclass = $("select").val();
			if (userid.length < 5) {
				alert("账号长度不能小于5位")
				return false;
			}
			if (username.length < 2) {
				alert("姓名长度不能低于2位")
				return false
			}
			if (userphone.length != 11) {
				alert("手机号格式不正确")
				return false;
			}
			return true;
		}

		$("form").submit(function() {
			$("#loading").css('display', "flex!important");
			if (!CheckForm()) {
				return false;
			}
			let forms = $("form").serialize();
			$.ajax({
				url : "Register",
				type : "post",
				dataType : "json",
				data : forms,
				success : function(res) {
					alert(res.msg)
				},
				complete : function() {
					$("#loading").css('display', "none!important");
				}
			})
			return false;
		})
	</script>
</body>

</html>