<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="icon" href="img/favicon.ico" />
    <title>
        <%
            request.setCharacterEncoding("utf-8");
            out.println(request.getParameter("title"));
        %>——党费缴费系统
    </title>
    <link rel="stylesheet" href="css/<%=request.getParameter("cssname")%>.css">

</head>